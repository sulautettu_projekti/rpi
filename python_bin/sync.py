#!/usr/bin/python2

import json
import MySQLdb
import urllib2
import time
import ConfigParser
import logging
import requests
from logging.config import fileConfig

fileConfig('/etc/sensor_cfg/log.cfg')
logger = logging.getLogger('Json_logger')

config = ConfigParser.RawConfigParser()
config.read('/etc/sensor_cfg/sensor.cfg')

local_db_length = (int)(config.get('sensor_cfg', 'LOCAL_DB_LENGTH'))
dbStart=((int)(time.time())-(local_db_length*60))

sql_host = config.get('mysql', 'HOST')
sql_user = config.get('mysql', 'USER')
sql_pass = config.get('mysql', 'PASSWORD')
sql_db = config.get('mysql', 'DATABASE')

server_host = config.get('server', 'HOST')

logger.debug("server_host: " + server_host)
logger.debug(" SQL Settings: \n\tHost: " + sql_host + "\n\tUser: " + sql_user + "\n\tDatabase: " + sql_db)

try:
	response=urllib2.urlopen(server_host,timeout=1)
	logger.info("Connection to host OK!")
except urllib2.URLError as err:
	logger.error("Couldn't connect to host!")
# 	quit()

update_cmd = "UPDATE synced SET synced.timestamp=%s WHERE id=1"
select_cmd = "SELECT timestamp, sensorId, temperature FROM SensorValues WHERE timestamp>%s ORDER BY timestamp"
delete_cmd = "DELETE FROM SensorValues WHERE timestamp<%s"

logger.debug("update_cmd: " + update_cmd)
logger.debug("select_cmd: " + select_cmd)

def sqlToJson(sqlInputData, columns):
        rowarray_list = []
        for row in sqlInputData:
		row = dict(zip(columns, row))
		rowarray_list.append(row)
        j = json.dumps(rowarray_list)
        return j

try:
	dbconn = MySQLdb.connect(host=sql_host,
               	user=sql_user,
                passwd=sql_pass,
                db=sql_db)

	dbcur = dbconn.cursor()
	dbcur.execute("SELECT timestamp FROM synced WHERE id=1")
	tmpTimestamp = dbcur.fetchone()[0]
	logger.debug(type(tmpTimestamp))
	logger.debug("tmptimestamp:" + str(tmpTimestamp))

	dbcur.execute(select_cmd,[tmpTimestamp])
	data = dbcur.fetchall()
	json_sql = sqlToJson(data, [desc[0] for desc in dbcur.description])
	logger.debug(json_sql)	
	r = requests.post(server_host,json_sql,verify=False)
	#logger.debug(r)
	#logger.debug(r.text)
	jsonResp = r.json()
	
	if (jsonResp['succeed']):
		logger.debug("Json succeeded.")
		dbcur.execute(update_cmd, [((int)(time.time()))])
       		dbcur.connection.commit()

	        ############# DELETE OLD ROWS #############
        
		dbcur.execute(delete_cmd, [dbStart])
        	logger.debug(dbStart)
        	logger.debug("delete_cmd: " + delete_cmd)
        	dbcur.connection.commit()

	else:
		logger.warn("Json upload failed! Reason: " + jsonResp['message'])


except MySQLdb.Error, e:
	logger.error(e)
finally:
	dbconn.close()

