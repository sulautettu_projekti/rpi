#!/usr/bin/python2
import serial
import json
import MySQLdb
import time
import ConfigParser
import logging

from logging.config import fileConfig



select_cmd = "SELECT timestamp, sensorId, temperature FROM SensorValues WHERE timestamp>%s ORDER BY timestamp"
update_cmd = "UPDATE btsynced SET btsynced.timestamp=%s WHERE deviceId=%s"


fileConfig('/etc/sensor_cfg/log.cfg')
logger = logging.getLogger('Json_logger')

config = ConfigParser.RawConfigParser()
config.read('/etc/sensor_cfg/sensor.cfg')

sql_host = config.get('mysql', 'HOST')
sql_user = config.get('mysql', 'USER')
sql_pass = config.get('mysql', 'PASSWORD')
sql_db = config.get('mysql', 'DATABASE')

def sqlToJson(sqlInputData, columns):
        rowarray_list = []
        for row in sqlInputData:
		row = dict(zip(columns, row))
		rowarray_list.append(row)
        j = json.dumps(rowarray_list)
        return j

port=serial.Serial("/dev/ttyAMA0", baudrate=115200, timeout=8.0)

id = ""
for i in range(0, 3):
	port.write("AT")
	device = ""
	time.sleep(1)
	logger.info(port.inWaiting())

	while(port.inWaiting() > 0):
		rcv = port.read()
		device += rcv
	
	logger.info(device)

	if (device == "OK" or len(device) < 5):
		logger.info("No bt device connected")
	else:
		try:
			dbconn = MySQLdb.connect(host=sql_host,
        		       	user=sql_user,
        		        passwd=sql_pass,
        	        	db=sql_db)
			dbcur = dbconn.cursor()
		
			while (not id):
				dbcur.execute("select deviceId from btdevices where device=%s", [str(device)])
				id = dbcur.fetchone()
				logger.info(type(id))
				logger.info(id)
				if(None in [id]):
					logger.info("Inserting device to db.")
					dbcur.execute("INSERT INTO btdevices(device) VALUES(%s)", [str(device)])
					dbconn.commit()
					dbcur.execute("select deviceId from btdevices where device=%s", [str(device)])
                                	id = dbcur.fetchone()

			dbcur.execute("SELECT timestamp FROM btsynced WHERE deviceId=%s", id)
	
			tmpTimestamp = dbcur.fetchone()
			logger.debug(type(tmpTimestamp))
			if(None in [tmpTimestamp]):
				tmpTimestamp = 0
			logger.debug("tmptimestamp:" + str(tmpTimestamp))
	
			dbcur.execute(select_cmd,[tmpTimestamp])
			data = dbcur.fetchall()
			if(len(data) > 200):
				data = data[(len(data)-200):]
				logger.info(len(data))
			logger.info(len(data))
			json_sql = sqlToJson(data, [desc[0] for desc in dbcur.description])
			#logger.info(json_sql)	
			port.write(json_sql)
			ackWaitTime = 0
			while(port.inWaiting() == 0 and ackWaitTime < 10):
				time.sleep(1)
				ackWaitTime+=1
				logger.info("Waiting for ack...")
			
			ack = ""
			while(port.inWaiting() > 0):
        		        tmp = port.read()
		                ack += tmp
						
			logger.info(ack)

			if(tmpTimestamp == 0):
				logger.debug("No timestamp for last sync, creating one.")
				dbcur.execute("INSERT INTO btsynced(timestamp, deviceId) VALUES(%s, %s)", [ack, id])
				dbconn.commit()
			else:
				logger.debug("Updating sync time")
				logger.info("Update cmd: " + update_cmd)
				logger.info("Ack, id" + str(ack) + " " + str(id))
				dbcur.execute(update_cmd, [ack, id])
				dbconn.commit()
			if (i != 2):
				logger.info("Sleeping for 15s.")
				time.sleep(15)
		except MySQLdb.Error, e:
			logger.error(e)
			dbconn.close()
			quit()

		finally:
			if(i == 2):
				logger.info("Closing db connection")
				dbconn.close()
