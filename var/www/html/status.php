<?php

/*** begin the session ***/
session_start();

if($_SESSION['user_id'] == false)
{
    ob_start();
    while (ob_get_status())
    {
      ob_end_clean();
    }
    header( "Location: index.php" );
} else {
$logfile = fopen("/var/tmp/json_logger.log","r");?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Status</title>
  <link rel="stylesheet" href="css/cascade.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<header>
   <div class="fill">
    <div class="container">
     <a class="brand">MBED Server</a>
     <ul class="nav">
       <li>
            <a href="status.php">Status</a>
	</li>
	<li><a href="config.php">Configuration</a></li>
	 <li>
	 <a href="logout.php">Logout</a>
        </li> 
	</ul>
    </div>
   </div>
  </header><div id="maincontent" class="container">

  <section class="container">
    <div class="configuration">
      <h1><a id="content">Status</a></h1>		
	
	<textarea rows="60" id="syslog" readonly="readonly">
	<?php
		
		while (!feof($logfile)) {
   			$line = fgets($logfile);
   			echo $line;
		}
		fclose($logfile);
	?>
	</textarea>
	</div>
    </div>
<section>
</body>
</html>
<?php
}
?>
