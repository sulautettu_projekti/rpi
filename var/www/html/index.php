<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Login</title>
  <link rel="stylesheet" href="css/cascade.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<header>
   <div class="fill">
    <div class="container">
     <a class="brand">MBED Server</a>
     <ul class="nav">
	</ul>
    </div>
   </div>
  </header><div id="maincontent" class="container">

  <section class="container">
    <div class="login">
      <h1>Login to MBED server configuration</h1>
      <?php echo "<p style=\"color:red\">", $_GET["login"], "<p>"; ?>
	<form method="post" action="auth.php">
        <label class="cbi-value-title">Username</label>
	<p><input type="text" name="username" value="root" placeholder="Username"></p>
	<label class="cbi-value-title">Password</label>
        <p><input type="password" name="password" value="" placeholder="Password"></p>
        <p class="submit"><input type="submit" name="commit" value="Login"></p>
      </form>
    </div>
<section>
</body>
</html>
