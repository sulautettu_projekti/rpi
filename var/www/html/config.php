<?php

/*** begin the session ***/
session_start();

if($_SESSION['user_id'] == false)
{
    ob_start();
while (ob_get_status())
{
      ob_end_clean();
     }
      header( "Location: index.php" );

} else {

$ini_array = parse_ini_file("/etc/sensor_cfg/sensor.cfg", true);


function write_php_ini($array, $file)
{
    $res = array();
    foreach($array as $key => $val)
    {
        if(is_array($val))
        {
            $res[] = "[$key]";
            foreach($val as $skey => $sval) $res[] = "$skey = $sval";
        }
        else $res[] = "$key = $val";
    }
    safefilerewrite($file, implode("\r\n", $res));
}

function safefilerewrite($fileName, $dataToSave)
{    if ($fp = fopen($fileName, 'w'))
    {
        $startTime = microtime(TRUE);
        do
        {            $canWrite = flock($fp, LOCK_EX);
           // If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
           if(!$canWrite) usleep(round(rand(0, 100)*1000));
        } while ((!$canWrite)and((microtime(TRUE)-$startTime) < 5));

        //file was locked so now we can store information
        if ($canWrite)
        {            fwrite($fp, $dataToSave);
            flock($fp, LOCK_UN);
        }
        fclose($fp);
    }
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Configuration</title>
  <link rel="stylesheet" href="css/cascade.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<header>
   <div class="fill">
    <div class="container">
     <a class="brand">MBED Server</a>
     <ul class="nav">
       <li>
        <a href="status.php">Status</a>
        </li>
	<li><a href="config.php">Configuration</a></li>
	 <li>
	 <a href="logout.php">Logout</a>
        </li> 
	</ul>
    </div>
   </div>
  </header><div id="maincontent" class="container">

  <section class="container">
    
	<h1><a id="content">Change root password</a></h1>
	<form action=?>
	<div class="cbi-value" id="cbi-system-_pass-pw1">
	<div class="cbi-value" id="cbi-system-_pass-pw1"><label class="cbi-value-title" for="cbid.system._pass.pw1">New password&nbsp;</label>
	<input type="password" class="cbi-input-password" name="pw" id="cbid.system._pass.pw1" value="" /><br><br>
        <div class="cbi-value" id="cbi-system-_pass-pw1"><label class="cbi-value-title" for="cbid.system._pass.pw1">Validate password&nbsp;</label>
                
        <input type="password" class="cbi-input-password" name="pwValid" id="cbid.system._pass.pw1" value="" /></div>
	</form>
      <h1><a id="content">Configuration</a></h1>
	
	<form action="<?php
	
	if(isset($_POST['cbi_apply'])){
	$ini_array = parse_ini_file("/etc/sensor_cfg/sensor.cfg", true);
	if (filter_var($_POST["host"], FILTER_VALIDATE_URL)) {
   		$ini_array["server"]["HOST"] = $_POST["host"];
	}
        if (filter_var($_POST["local_db_length"],FILTER_VALIDATE_INT, array('options' => array( 'min_range' => 300, 'max_range' => 5760))))  {
                $ini_array["sensor_cfg"]["LOCAL_DB_LENGTH"] = $_POST["local_db_length"];
        }
	if (!(filter_var($_POST["car_id"], FILTER_VALIDATE_INT,array('options' => array('min_range' => 0))) === False)) {
                $ini_array["sensor_cfg"]["CAR_ID"] = $_POST["car_id"];
        }
	
	write_php_ini($ini_array, "/etc/sensor_cfg/sensor.cfg");
}
 ?>" method="post">
	<div class="cbi-value cbi-value-last" id="cbi-system-_pass-pw2"><label class="cbi-value-title" for="cbid.system._pass.pw2">Local database history (seconds)&nbsp;</label>
	<input type="number" name="local_db_length" value="<?php echo $ini_array["sensor_cfg"]["LOCAL_DB_LENGTH"]?>"/>
		
		<br><br>
		<label class="cbi-value-title" for="cbid.system._pass.pw2">Host&nbsp;</label>
                <input type="text" name="host" value="<?php echo $ini_array["server"]["HOST"]?>"/>
		<br><br>
                <label class="cbi-value-title" for="cbid.system._pass.pw2">Car ID&nbsp;</label>
                <input type="number" name="car_id" value="<?php echo $ini_array["sensor_cfg"]["CAR_ID"]?>"/>
	</div>
<br>
<div class="cbi-page-actions">	
	<input class="cbi-button cbi-button-apply" type="submit" name="cbi.apply" value="Save &#38; Apply" />
	</form>
	</div>
<section>
</body>
</html>
<?php
}
?>
