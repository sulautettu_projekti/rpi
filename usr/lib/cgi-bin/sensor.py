#!/usr/bin/env python
import cgi
import MySQLdb
import os
import ConfigParser

print

######### CONFIG ##########

config = ConfigParser.RawConfigParser()
config.read('/etc/sensor_cfg/sensor.cfg')

sql_host = config.get('mysql', 'HOST')
sql_user = config.get('mysql', 'USER')
sql_pass = config.get('mysql', 'PASSWORD')
sql_db = config.get('mysql', 'DATABASE')

##########################



form = cgi.FieldStorage()
if (None in (form.getvalue("timestamp"), form.getvalue("sensorId"), form.getvalue("temperature"))): 
	print("Error, one of the values: timestamp, sensordId or temperature was None")
else:
	sql_cmd = "INSERT INTO SensorValues(timestamp, sensorId, temperature) VALUES(%s, %s, %s)"
	if form.getvalue("timestamp") != '0':
		
		args = form.getvalue("timestamp"), form.getvalue("sensorId"), form.getvalue("temperature")	
	else:
		print("timestamp 0, using local timestamp.")
		args = str(int(time.time())), form.getvalue("sensorId"), form.getvalue("temperature")
	try:
		dbconn = MySQLdb.connect(host=sql_host,
                	user=sql_user,
                  	passwd=sql_pass,
                  	db=sql_db)
	
		dbcur = dbconn.cursor()
		dbcur.execute(sql_cmd, args)
		dbconn.commit()
	except MySQLdb.Error, e:
		print(e)
	finally:
		dbconn.close()
