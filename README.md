RPi:n välityspalvelimen tiedostot:

* NTP serveri
* DHCP serveri
* Apache2
* MBED konfiguraatiosivusto: https://ollanketo.ddns.net:8081/
    * sync.py:n logi
    * Lokaalin tietokannan historian muutos 120-5760s
    * CAR_ID 0->
    * Vastaanottopalvelimen osoite

* Datan vastaanotto: http://ollanketo.ddns.net:8080/sensor.py

sensor.py ottaa vastaan mbediltä POST:lla tulevan sensoridatan.
sync.py synkronoi lokaalin tietokannan palvelimelle. Sync.py skripti ajetaan cron-jobina minuutin välein.

/var/lib/www kansiossa on konfiguraatiosivuston lähdekoodi.